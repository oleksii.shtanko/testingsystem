﻿using KnowledgeTestingSystemWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly TestingSystemContext _context;

        public AdminController(IConfiguration config, TestingSystemContext context)
        {
            _config = config;
            _context = context;
        }

        [HttpGet]
        public IActionResult Login(string username, string pass)
        {
            Admin auth = new Admin
            {
                Login = username,
                Password = pass
            };
            IActionResult response = Unauthorized();

            var admin = AuthenticateUser(auth);
            if (admin != null)
            {
                var tokenStr = GenerateJSONWebToken(admin);
                response = Ok(new { token = tokenStr });
            }
            return response;
        }
        private Admin AuthenticateUser(Admin auth)
        {
            Admin admin = null;
            byte[] ByteData = Encoding.ASCII.GetBytes(auth.Password);
            MD5 md5 = MD5.Create();
            byte[] HashData = md5.ComputeHash(ByteData);
            StringBuilder oSb = new StringBuilder();
            for (int x = 0; x < HashData.Length; x++)
            {
                oSb.Append(HashData[x].ToString("x2"));
            }
            auth.Password = oSb.ToString();
            Admin adminData = _context.DbAdmins.FirstOrDefault(u => u.Login == auth.Login &&
            u.Password == auth.Password);
            if (adminData != null)
            {
                if (auth.Login == adminData.Login && auth.Password == adminData.Password)
                {
                    admin = new Admin { Login = adminData.Login, Password = adminData.Password, Id = adminData.Id };
                }
                return admin;
            }
            return null;
        }


        private string GenerateJSONWebToken(Admin superAdminInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,superAdminInfo.Login),
                new Claim(JwtRegisteredClaimNames.Email,superAdminInfo.Id.ToString())
        };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            var encodeToken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodeToken;
        }
        [Authorize]
        [HttpPost("Post")]
        public string Post()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = identity.Claims.ToList();
            var adminName = claim[1].Value;
            return adminName;
        }
        [HttpPost("CreateAdmin")]
        public async Task<ActionResult<Admin>> PostAdmin(Admin admin)
        {
            Admin isLoginExcist = _context.DbAdmins.FirstOrDefault(u => u.Login == admin.Login);
            if (isLoginExcist == null)
            {
                byte[] ByteData = Encoding.ASCII.GetBytes(admin.Password);
                MD5 md5 = MD5.Create();
                byte[] HashData = md5.ComputeHash(ByteData);
                StringBuilder oSb = new StringBuilder();
                for (int x = 0; x < HashData.Length; x++)
                {
                    oSb.Append(HashData[x].ToString("x2"));
                }
                admin.Password = oSb.ToString();
                _context.DbAdmins.Add(admin);
                await _context.SaveChangesAsync();
                return CreatedAtAction("PostAdmin", new { id = admin.Id }, admin);
            }
            return BadRequest("Login already exists.");
        }
    }
}
