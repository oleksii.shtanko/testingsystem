﻿using KnowledgeTestingSystemWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly TestingSystemContext _context;
        public QuestionController(TestingSystemContext context)
        {
            _context = context;
        }

        // GET: api/Test
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TestQuestion>>> GetTestQuestions()
        {
            return await _context.DbQuestions.ToListAsync();
        }

        // GET: api/Test/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TestQuestion>> GetTest(int id)
        {
            var question = await _context.DbQuestions.FindAsync(id);

            if (question == null)
            {
                return NotFound();
            }

            return question;
        }

        [HttpPost]
        public async Task<ActionResult<Test>> PostQuestion(TestQuestion test)
        {
            _context.DbQuestions.Add(test);
            await _context.SaveChangesAsync();

            return CreatedAtAction("PostQuestion", new { id = test.Id }, test);
        }
    }
}
