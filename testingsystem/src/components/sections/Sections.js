import React,{useState}  from "react"
import {Route} from 'react-router-dom'
import LoginPage from '../loginPage/LoginPage'
import Logout from '../logout/Logout'
import RegistrationPage from "../registrationPage/RegistrationPage"
import TestCreateComponent from "../adminComponents/testCreateComponent/TestCreateComponent"
import ConfirmeTestComponent from "../adminComponents/confirmeTestComponent/ConfirmeTestComponent"
import AddQuestionComponent from "../adminComponents/addQuestionComponent/AddQuestionComponent"
import TestListComponent from "../userComponents/testListComponent/TestListComponent"
import StartTestComponent from "../userComponents/startTestComponent/StartTestComponent"
import PersonalDataComponent from "../userComponents/userProfileComponent/PersonalDataComponent"
import TestResultComponent from "../userComponents/userProfileComponent/TestResultComponent"
import HomePageComponent from "../homePageComponent/HomePageComponent"
const  Sections=(props)=> {

    return(
        <section>
            <Route  path="/login"  render={()=><LoginPage  userAuth={props.userAuth} Admin={props.Admin}/>}/>
            <Route  path="/logout"  render={()=><Logout userAuth={props.userAuth} Admin={props.Admin} />}/>
            <Route  exact path="/"  render={()=><HomePageComponent  userAuth={props.userAuth} Admin={props.Admin}/>}/>
            <Route  path="/registration"  render={()=><RegistrationPage userAuth={props.userAuth} Admin={props.Admin} />}/>
            <Route  path="/createTest"  render={()=><TestCreateComponent userAuth={props.userAuth} Admin={props.Admin} />}/>
            <Route  path="/confirmeTests"  render={()=><ConfirmeTestComponent userAuth={props.userAuth} Admin={props.Admin} />}/>
            <Route  path="/addQuestion/:id"  component={AddQuestionComponent}/>
            <Route  path="/startTest/:id"  component={StartTestComponent} />
            <Route  path="/tests"  render={()=><TestListComponent userAuth={props.userAuth} Admin={props.Admin} />}/>
            <Route  path="/personalData"  render={()=><PersonalDataComponent userAuth={props.userAuth} Admin={props.Admin} />}/>
            <Route  path="/testResult"  render={()=><TestResultComponent userAuth={props.userAuth} Admin={props.Admin} />}/>
        </section>
      
    )    
}
export default Sections