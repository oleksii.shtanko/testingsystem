﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Models
{
    public class Test
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "int")]
        public int CreatorId { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string Description { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string Type { get; set; }
        [Column(TypeName = "int")]
        public int MaximumAttemptByUser { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Password { get; set; }
        [Column(TypeName = "int")]
        public int PassingScore { get; set; }
        [Column(TypeName = "int")]
        public int MaximumScore { get; set; }
        [Column(TypeName = "BIT")]
        public bool IsDeleated { get; set; }
    }
}
