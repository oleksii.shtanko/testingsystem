﻿using KnowledgeTestingSystemWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestHistoryController : ControllerBase
    {
        private readonly TestingSystemContext _context;

        public TestHistoryController(TestingSystemContext context)
        {
            _context = context;
        }
        [HttpGet("Test/{userId}")]
        public ActionResult<IEnumerable<TestHistory>> GetTestHistorysByUser(int userId)
        {

            List<TestHistory> testHistoryContext = null;
            foreach (var testHistory in _context.DbTestHistories)
            {
                if (testHistory.UserId == userId)
                {
                    testHistoryContext.Add(testHistory);
                }
            }
            return testHistoryContext;
        }
        [HttpPost]
        public async Task<ActionResult<TestHistory>> PostTestHistory(TestHistory testHistory)
        {
            _context.DbTestHistories.Add(testHistory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("PostTestHistory", new { id = testHistory.Id }, testHistory);
        }
    }
}
