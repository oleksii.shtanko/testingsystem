import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import {NavLink,Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from "axios"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import MenuItem from '@material-ui/core/MenuItem';
import 'react-notifications/lib/notifications.css';
import { Redirect } from "react-router-dom"

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
  },
  container:{
    paddingBottom:"300px"
  }
}));

const RegistrationPage=(props)=> {
  const classes = useStyles();
  
  const selectSex = [
    {
      value: 'Man',
      label: 'Man',
    },
    {
      value: 'Woman',
      label: 'Woman',
    },
  ];
  const [currency, setCurrency] = useState('');
  const initialFieldValues={
    login:'',
    password:'',
    fullName:'',
    phone:'',
    age:'',
    sex:'',
    isDeleated:false
}
const [UserData, setUserData] = useState(initialFieldValues)
const [DirtyLogin,setDirtyLogin]=useState(false)
const [loginError,setLoginError]=useState("Логин некоректный")
const [DirtyPassword,setDirtyPassword]=useState(false)
const [passwordError,setPasswordError]=useState("Пароль некоректный")
const [DirtyEmail,setDirtyEmail]=useState(false)
const [emailError,setEmailError]=useState("Email некоректный")
const [formValid,setFromValid]=useState(false)

const handleChangeLogin = (event) => {
  const re = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
  if(!re.test(String(event.target.value).toLowerCase())){
    setLoginError("Логин некоректний")
  }
  else{
    setLoginError("")
  }
  setUserData({
    ...UserData,
    login:event.target.value,
  });
};
const handleChangePassword = (event) => {
  const re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
  if(!re.test(String(event.target.value).toLowerCase())){
    setPasswordError("Пароль некоректний")
  }
  else{
    setPasswordError("")
  }
  setUserData({
    ...UserData,
    password:event.target.value,
  });
};
const handleChangeFullName = (event) => {
  setUserData({
    ...UserData,
    fullName:event.target.value,
  });
};
const handleChangePhone = (event) => {
  setUserData({
    ...UserData,
    phone:event.target.value,
  });
};
const handleChangeEmail = (event) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(!re.test(String(event.target.value).toLowerCase())){
    setEmailError("Email некоректний")
  }
  else{
    setEmailError("")
  }
  setUserData({
    ...UserData,
    email:event.target.value,
  });
};
const handleChangeAge = (event) => {
  if(UserData.age>=0)
  {
  setUserData({
    ...UserData,
    age:event.target.value,
  });
  
}
};
const handleChange = (event) => {
  setCurrency(event.target.value);
};
var createNotification = (type) => {
  
    switch (type) {
      case 'success':
        NotificationManager.success('Account created', 'Success',500);
        break;
      case 'error':
        NotificationManager.error('Login already exist', 'Error', 500);
        break;
    }

};


  const handleSubmit=(e)=>
  {
   e.preventDefault();
        axios
        .post(
           `https://localhost:5001/api/User/CreateUser`,
           {
            "login": UserData.login,
            "password":UserData.password,
            "fullName":UserData.fullName,
            "phone": UserData.phone,
            "email":UserData.email,
            "age":parseInt(UserData.age),
            "sex":currency,
            "isDeleated":UserData.isDeleated,
           }
        ).then(res=>{
      createNotification('success')
        }).catch(err=>
        {
          createNotification('error')
        })
}

  useEffect(() => {
    if(loginError ||passwordError||emailError||UserData.phone=='' || UserData.age==''){
      setFromValid(false)
    }
    else{
      setFromValid(true)
    }
  },[loginError,passwordError,emailError,UserData]);

  const blurHandler=(e)=>{
    switch(e.target.name){
      case 'login':
        setDirtyLogin(true)
    break
    case 'password':
      setDirtyPassword(true)
    break
    case 'email':
      setDirtyEmail(true)
    break
    }
      }

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
       
        </Avatar>
        <Typography component="h1" variant="h5">
        Sign Up
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="fullName"
            label="Full Name"
            name="fullName"
            autoComplete="fullName"
            autoFocus
            value={UserData.fullName}
            onChange={handleChangeFullName}
            
          />
           {(DirtyLogin && loginError)&&<div style={{color:'red'}}>{loginError}</div>}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="login"
            label="Login"
            type="login"
            id="login"
            autoComplete="current-login"
            value={UserData.login}
            onChange={handleChangeLogin}
            onBlur={e=>blurHandler(e)}
          />
           {(DirtyPassword && passwordError)&&<div style={{color:'red'}}>{passwordError}</div>}
            <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={UserData.password}
            onChange={handleChangePassword}
            onBlur={e=>blurHandler(e)}
          />
           {(DirtyEmail && emailError)&&<div style={{color:'red'}}>{emailError}</div>}
            <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="email"
            label="Email"
            type="email"
            id="email"
            autoComplete="current-email"
            value={UserData.email}
            onChange={handleChangeEmail}
            onBlur={e=>blurHandler(e)}
          />
               <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="age"
            label="Age"
            type="number"
            id="age"
            min="0"
            value={UserData.age}
            onChange={handleChangeAge}
          />
             <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="phone"
            label="Phone"
            name="phone"
            autoComplete="phone"
            autoFocus
            value={UserData.phone}
            onChange={handleChangePhone}
          />
          
          <TextField
            margin="normal"
          id="outlined-select-currency"
          required
          select
          label="Select"
          fullWidth
          value={currency}
          onChange={handleChange}
          variant="outlined"
        >
          {selectSex.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>

          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
            disabled={!formValid}
          >
           Sign Up
          </Button>
          <NotificationContainer/>
          <Grid container  alignItems="center"
  justify="center">
            <Grid item >
              <Link to="/login" variant="body2" >
              Back to sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
export default RegistrationPage