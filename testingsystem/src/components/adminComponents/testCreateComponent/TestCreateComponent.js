import React,{useState,useEffect} from "react"
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from "axios"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  btn:{
    marginTop:theme.spacing(2)
  }
}));
const TestCreateComponent=()=>{
    const classes = useStyles();

    const initialFieldValues={
      Name:'',
      Description:'',
      Type:'',
      MaximumAttemptByUser:'',
      Timer:'',
      Password:'',
  }
  const [testData, setTestData] = useState(initialFieldValues)
  var createNotification = (type) => {
  
    switch (type) {
      case 'success':
        NotificationManager.success('Success', 'Success',500);
        break;
      case 'error':
        NotificationManager.error('Sothing went wrong', 'Error', 500);
        break;
    }

};
const handleChangeName=(event)=>{
  setTestData({
    ...testData,
    Name:event.target.value,
  });
}
const handleChangeDescription=(event)=>{
  setTestData({
    ...testData,
    Description:event.target.value,
  });
}
const handleChangeMaximumAttemptByUser=(event)=>{
  setTestData({
    ...testData,
    MaximumAttemptByUser:event.target.value,
  });
}
const handleChangePassword=(event)=>{
  setTestData({
    ...testData,
    Password:event.target.value,
  });
}
const handleChangeTimer=(event)=>{
  setTestData({
    ...testData,
    Timer:event.target.value,
  });
}
const handleChangeType=(event)=>{
  setTestData({
    ...testData,
    Type:event.target.value,
  });
}
const createTest=(e)=>
{
 e.preventDefault();
 axios
 .post(
    `https://localhost:5001/api/Test`,{
    "CreatorId":parseInt(localStorage.getItem("adminId")),
    "Name":testData.Name,
    "Description":testData.Description,
    "Type":testData.Type,
    "MaximumAttemptByUser":parseInt(testData.MaximumAttemptByUser),
    "Password":testData.Password,
    "Timer":parseInt(testData.Timer),
    "Status":'Awaiting confirmation',
    "isDeleated":false}).then(response => {
      createNotification('success')
      console.log(response)
    }).catch(err=>
      {
        console.log(err)
        createNotification('error')
      })
}


    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={2}>
            <Paper className={classes.paper}>xs=12</Paper>
          </Grid>
          <Grid item xs={8}>
            <Paper className={classes.paper}>
            <Grid container >
                <Grid item xs={3}></Grid>
                <Grid item xs={6}>
                    <h1>Create Test</h1>
                    <h3>Test name</h3>
        <TextField
          required
          fullWidth
          id="test-name"
          label="Required"
          value={testData.Name}
          onChange={handleChangeName}
          variant="outlined"/>
          <h3>Test Type</h3>
            <TextField
          id="test-description"
          label="Type"
          fullWidth
          required
          value={testData.Type}
          onChange={handleChangeType}
          variant="outlined"/>
            <h3>Test description</h3>
            <TextField
          id="test-description"
          label="Description"
          fullWidth
          required
          multiline
          rows={4}
          value={testData.Description}
          onChange={handleChangeDescription}
          variant="outlined"/>
             <h3>Test password</h3>
            <TextField
          required
          fullWidth
          id="outlined-required-test"
          label="Required"
          value={testData.Password}
          onChange={handleChangePassword}
          variant="outlined"/>
           <h3>Maximum attempt by user</h3>
            <TextField
           required
           fullWidth
           id="max-attept"
           label="Required"
           value={testData.MaximumAttemptByUser}
           onChange={handleChangeMaximumAttemptByUser}
           variant="outlined"/>
           <h3>Time</h3>
            <TextField
           required
           fullWidth
           id="timer"
           label="Required"
           value={testData.Timer}
           type="number"
           onChange={handleChangeTimer}
           variant="outlined"/>
                </Grid>
                      <Grid container>
                      
                      <Grid item xs={5}>
                      </Grid>
                      <Grid item xs={2}>
                      <Button
                      className={classes.btn}
                     type="submit"
                     fullWidth
                     variant="contained"
                    color="primary"
                    onClick={createTest}>
                      Create test
                     </Button>
                      </Grid>
                      <Grid item xs={5}>
                      </Grid>
            <Grid item xs={3}></Grid>
        </Grid>
       
        <Grid item xs={3}></Grid>
        </Grid>
            </Paper>
          </Grid>
          <Grid item xs={2}>
            <Paper className={classes.paper}>xs=6</Paper>
          </Grid>
          <NotificationContainer/>
        </Grid>
      </div>
    );
}

export default TestCreateComponent