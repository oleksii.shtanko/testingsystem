import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import axios from "axios"
import Button from '@material-ui/core/Button';
import { Redirect } from "react-router-dom"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    margin:theme.spacing(2),
    padding: theme.spacing(3),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  selectedPage:{
    margin:theme.spacing(2),
    padding: theme.spacing(3),
    textAlign: 'center',
     background: "#3d50b6",
     color:"white"
  },
  linkSelected:{
    color:"white"
  },
  btn:{
    marginTop:theme.spacing(2)
  }

}));
const PersonalDataComponent =(props)=>{
    const classes = useStyles();
    const [UserData, setUserData] = useState({
      login:"",
      email:"",
      fullName:"",
      password:"",
      phone:"",
      sex:" ",
      age:"",
      })
    const [password,setPassword]=useState({
      newPassword:"",
      repeateNewPassword:""
    })
    const [DirtyPassword,setDirtyPassword]=useState(false)
    const [passwordError,setPasswordError]=useState("Пароль некоректний")
    const [formValid,setFromValid]=useState(false)
    const setProfileInfo=()=>{
      axios
      .get(
         `https://localhost:5001/api/User/${localStorage.getItem('id')}`).then(response => {
          setUserData(response.data)
         })}

        
    const ChangePassword=()=>{
      console.log(password)
      if(password.newPassword==password.repeateNewPassword){
        axios.put(`https://localhost:5001/api/User/${localStorage.getItem('id')}`,{...UserData,password:password.newPassword}).then(response=>{
          createNotification('success')
        }).catch(err=>
          {
            createNotification('error')
          })}
          if(password.newPassword1=password.repeateNewPassword){
            createNotification('not match')
          }

    }

    const handleChangeNewPassword=(event)=>{
      const re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if(!re.test(String(event.target.value).toLowerCase())){
      setPasswordError("Пароль некоректний")
    }
    else{
      setPasswordError("")
    }
      setPassword({
        ...password,
        newPassword:event.target.value,
      });
    }
    const handleChangeRepeateNewPassword = (event) => {
      setPassword({
        ...password,
        repeateNewPassword:event.target.value,
      });
    };
    useEffect(() => {
      if(passwordError){
        setFromValid(false)
      }
      else{
        setFromValid(true)
      }
      setProfileInfo()
      },[passwordError]);

      const blurHandler=(e)=>{
        switch(e.target.name){
        case 'password':
          setDirtyPassword(true)
        break
        }
      }

      var createNotification = (type) => {
         switch (type) {
          case 'success':
            NotificationManager.success('Success', 'Success',500);
            break;
          case 'error':
            NotificationManager.error('Please check your login and password', 'Error', 500);
            break;
            case 'not match':
              NotificationManager.error('Passwords do not match', 'Error', 500);
              break;
        }
    };

  if(props.userAuth.loggedIn==false )
  return(
      <Redirect to="/login"></Redirect>
     )
    return(
    <Grid container >
        <Grid item xs={3}>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>
              <Grid container>
                <Grid item xs={1}></Grid>
                <Grid item xs={5}>
                    <Link className={classes.linkSelected} to="/personalData"> 
                        <Paper className={classes.selectedPage}>
                            Personal data
                        </Paper>
                    </Link>
                </Grid>
                <Grid item xs={5}>
                    <Link className={classes.linkSelected} to="/testResult"> 
                        <Paper className={classes.paper}>
                            Test Results
                        </Paper>
                    </Link>
                </Grid>
                <Grid item xs={1}></Grid>
              </Grid>
            <Grid container>
              <Grid item xs={6}>
                <h2>Personal information</h2>
                <h4>Login</h4>
                <TextField
                  
                  disabled
                  id="outlined-disabled"
                  label="Login"
                  value={UserData.login}
                  variant="outlined"
                />
                <h4>Email</h4>
                   <TextField
                 
                  disabled
                  id="outlined-disabled"
                  label="Email"
               
                  value={UserData.email}
                  variant="outlined"
                />
                <h4>Full name</h4>
                   <TextField
                    
                  disabled
                  id="outlined-disabled"
                  label="Full name"
                  value={UserData.fullName}
                  variant="outlined"
                />
                <h4>Phone number</h4>
                   <TextField
                
                  disabled
                  id="outlined-disabled"
                  label="Phone"
                  value={UserData.phone}
                  variant="outlined"
                />
                <h4>Sex</h4>
                   <TextField
                    
                  disabled
                  id="outlined-disabled"
                  label="Sex"
                  value={UserData.sex}
                  variant="outlined"
                />
                <h4>Age</h4>
                   <TextField
                  disabled
                  id="outlined-disabled"
                  label="Age"
                  value={UserData.age}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={6}>
              <h2>Change password</h2>
              <h4>New password</h4>
              <TextField
              type="password"
              required
              id="current-password"
              label="Required"
              value={password.newPassword}
              onChange={handleChangeNewPassword}
              variant="outlined"
              onBlur={e=>blurHandler(e)}
              />
              {(DirtyPassword && passwordError)&&<div style={{color:'red'}}>{passwordError}</div>}
               <h4>Repeat new password</h4>
              <TextField
               type="password"
              required
              id="new-password"
              label="Required"
              value={password.repeateNewPassword}
              onChange={handleChangeRepeateNewPassword}
              variant="outlined"
              />
              <Grid container>
                <Grid item xs={3}></Grid>
                <Grid item xs={6}>
                <Button   className={classes.btn} disabled={!formValid} onClick={ChangePassword} fullWidth type="submit" variant="contained" color="primary">Change password</Button>
                </Grid> 
                <Grid item xs={3}></Grid>
                </Grid>
              </Grid>
            
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={3}>
        </Grid>
        <NotificationContainer/>
    </Grid>
    
    );
}
export default PersonalDataComponent