import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Image from '../../img/logo.svg';
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,

    },
paper:{
marginTop:theme.spacing(12)
},
    HeadBoxText:{
      padding: theme.spacing(2),
      textAlign:'center',
      fontSize:"42px",
    },
    img:{
      marginLeft:theme.spacing(6),
      maxWidth:"80%"

    }
  }));
  const  HomePageComponent=()=> {
    const classes = useStyles();
    return (
      <div className={classes.root}>
      <Grid container className={classes.paper} >
        <Grid item xs={2} sm={2}>
          
        </Grid>
        <Grid item xs={8} sm={8}>
          <Paper>
        <Grid container >
        <Grid item xs={6} >
            <p className={classes.HeadBoxText}>Workify -
            DDDD D DD  D DD </p>
          </Grid>
          <Grid item xs={6} >
           <img src={Image} className={classes.img} />
          </Grid>
          </Grid>
          </Paper>
        </Grid>
  
        <Grid item xs={2} sm={2}>
        </Grid>
      </Grid>
    </div>
    );
  }
export default HomePageComponent;