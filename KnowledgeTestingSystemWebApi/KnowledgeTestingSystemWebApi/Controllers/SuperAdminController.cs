﻿using KnowledgeTestingSystemWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperAdminController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly TestingSystemContext _context;

        public SuperAdminController(IConfiguration config, TestingSystemContext context)
        {
            _config = config;
            _context = context;
        }

        [HttpGet]
        public IActionResult Login(string username, string pass)
        {
            SuperAdmin auth = new SuperAdmin
            {
                Login = username,
                Password = pass
            };
            IActionResult response = Unauthorized();

            var superAdmin = AuthenticateUser(auth);
            if (superAdmin != null)
            {
                var tokenStr = GenerateJSONWebToken(superAdmin);
                response = Ok(new { token = tokenStr });
            }
            return response;
        }
        private SuperAdmin AuthenticateUser(SuperAdmin auth)
        {
            SuperAdmin superAdmin = null;
            byte[] ByteData = Encoding.ASCII.GetBytes(auth.Password);
            MD5 md5 = MD5.Create();
            byte[] HashData = md5.ComputeHash(ByteData);
            StringBuilder oSb = new StringBuilder();
            for (int x = 0; x < HashData.Length; x++)
            {
                oSb.Append(HashData[x].ToString("x2"));
            }
            auth.Password = oSb.ToString();
            SuperAdmin superAdminData = _context.DbSuperAdmins.FirstOrDefault(u => u.Login == auth.Login &&
            u.Password == auth.Password);
            if (superAdminData != null)
            {
                if (auth.Login == superAdminData.Login && auth.Password == superAdminData.Password)
                {
                    superAdmin = new SuperAdmin { Login = superAdminData.Login, Password = superAdminData.Password, Id = superAdminData.Id };
                }
                return superAdmin;
            }
            return null;
        }


        private string GenerateJSONWebToken(SuperAdmin superAdminInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,superAdminInfo.Login),
                new Claim(JwtRegisteredClaimNames.Email,superAdminInfo.Id.ToString())
        };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            var encodeToken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodeToken;
        }
        [Authorize]
        [HttpPost("Post")]
        public string Post()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = identity.Claims.ToList();
            var superAdminName = claim[1].Value;
            return superAdminName;
        }
        [HttpPost("CreateSuperAdmin")]
        public async Task<ActionResult<SuperAdmin>> PostSuperAdmin(SuperAdmin superAdmin)
        {
            SuperAdmin isLoginExcist = _context.DbSuperAdmins.FirstOrDefault(u => u.Login == superAdmin.Login);
            if (isLoginExcist == null)
            {
                byte[] ByteData = Encoding.ASCII.GetBytes(superAdmin.Password);
                MD5 md5 = MD5.Create();
                byte[] HashData = md5.ComputeHash(ByteData);
                StringBuilder oSb = new StringBuilder();
                for (int x = 0; x < HashData.Length; x++)
                {
                    oSb.Append(HashData[x].ToString("x2"));
                }
                superAdmin.Password = oSb.ToString();
                _context.DbSuperAdmins.Add(superAdmin);
                await _context.SaveChangesAsync();
                return CreatedAtAction("PostSuperAdmin", new { id = superAdmin.Id }, superAdmin);
            }
            return BadRequest("Login already exists.");
        }
    }
}
