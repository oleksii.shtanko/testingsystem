import React,{useState,useEffect} from 'react';
import { Redirect } from "react-router-dom"
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import axios from "axios"
import {Link} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import MenuItem from '@material-ui/core/MenuItem';
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      margin:theme.spacing(3),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    paperImg: {
      padding: theme.spacing(2),
  
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    head:{
      marginBottom: theme.spacing(5),
      marginTop: theme.spacing(5),
      textAlign:'center',
      fontSize:"42px",
  
    },
    img:{
      maxWidth:"100%"
  
    },
    about:{
      textAlign:'center',
      fontWeight:"bold"
    },
    main:{
     
    },
    button:{
      marginLeft:theme.spacing(2),
      padding: theme.spacing(2),
  marginTop:theme.spacing(2),
    }
  }));
const TestListComponent=()=>{
    const classes = useStyles();
    const [tests, setTests] = useState([]);
    const [currentSearch, setCurrentSearch] = useState([]);
    const handleChangeSearch = (event) => {
      setCurrentSearch(event.target.value);
    };
    const fetchData = () => { 
        axios
        .get(
           `https://localhost:5001/api/Test/Confirmed`).then(response => {
            setTests(response.data.reverse())
           })}
    const searchTest=()=>{
      if(currentSearch!='')
      {
      axios
      .get(
         `https://localhost:5001/api/Test/Confirmed/${currentSearch}`).then(response => {
          setTests(response.data.reverse())
          console.log(response)
         })
        }
        if(currentSearch==''){
          axios
        .get(
           `https://localhost:5001/api/Test/Confirmed`).then(response => {
            setTests(response.data.reverse())
           })
        }
    }
    useEffect(() => {
        fetchData();
       },[]);

return(
    <div>
      <Grid container>
        <Grid item xs={5}></Grid>
        <Grid item xs={2}>
<TextField
            variant="outlined"
            margin="normal"
            id="search"
            label="Search type"
            name="search"
            onChange={handleChangeSearch}
            value={currentSearch}
          />
          <Button onClick={searchTest} className={classes.button} type="submit" variant="contained" color="primary">Start</Button>
          </Grid>
          <Grid item xs={5}></Grid>
          </Grid>
{
    tests.map((data)=>
              {
                return(
                    <Grid  key={data.id} container >
                        <Grid item xs={3} sm={3}>
                        </Grid>
                        <Grid item xs={6}>
                    <Paper className={classes.paper}>
                  <Grid container>
                  <Grid item xs={3} >
                <h4 className={classes.about}>Test name</h4>
                <h1 >{data.name}</h1>
                </Grid>
                <Grid item xs={3} >
                <h4 className={classes.about}>Test type</h4>
                <h1 >{data.type}</h1>
                </Grid>
                <Grid item xs={6}>
                <h4 className={classes.about}>Test description</h4>
                <h1 >{data.description}</h1>
                </Grid>
                
                <Grid item xs={4}>
                </Grid>
                <Grid item xs={4}>
                <h1 >{data.adress}</h1>
                <Link to={`/startTest/${data.id}`} className="btn btn-primary" ><Button type="submit" variant="contained" color="primary">Start</Button></Link>
                </Grid>
                <Grid item xs={4}>          
                </Grid>
                </Grid>
                <Grid item xs={12}>
                  </Grid>
                </Paper>
                </Grid>
                <NotificationContainer/>
              </Grid>
)
              })
              }
    </div>
)
}
export default TestListComponent