﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Models
{
    public class TestQuestion
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "int")]
        public int TestId { get; set; }
        [Column(TypeName = "nvarchar(20)")]
        public string Type { get; set; }
        [Column(TypeName = "nvarchar(300)")]
        public string Question { get; set; }
        [Column(TypeName = "nvarchar(300)")]
        public string FirstAnswer { get; set; }
        [Column(TypeName = "nvarchar(300)")]
        public string SecondAnswer { get; set; }
        [Column(TypeName = "nvarchar(300)")]
        public string ThirdAnswer { get; set; }
        [Column(TypeName = "nvarchar(300)")]
        public string FourthAnswer { get; set; }
        [Column(TypeName = "BIT")]
        public bool IsFirstCorrect { get; set; }
        [Column(TypeName = "BIT")]
        public bool IsSecondCorrect { get; set; }
        [Column(TypeName = "BIT")]
        public bool IsThirdCorrect { get; set; }
        [Column(TypeName = "BIT")]
        public bool IsFourthCorrect { get; set; }

        [Column(TypeName = "int")]
        public int MaximumScoreExist { get; set; }

        [Column(TypeName = "BIT")]
        public bool IsDeleated { get; set; }

    }
}
