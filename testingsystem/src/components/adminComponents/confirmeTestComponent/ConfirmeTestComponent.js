import React,{useState,useEffect} from "react"
import axios from "axios"
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      margin:theme.spacing(3),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    paperImg: {
      padding: theme.spacing(2),
  
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    head:{
      marginBottom: theme.spacing(5),
      marginTop: theme.spacing(5),
      textAlign:'center',
      fontSize:"42px",
  
    },
    img:{
      maxWidth:"100%"
  
    },
    about:{
      textAlign:'center',
      fontWeight:"bold"
    },
    main:{
     
    },
    button:{
      padding: theme.spacing(3),
  
        hight:"200px"
    }
  }));
const ConfirmeTestComponent=()=>{
const classes = useStyles();
const [testsWaitingConfrime, settestsWaitingConfrime] = useState([])
var createNotification = (type) => {
  
    switch (type) {
      case 'success':
        NotificationManager.success('Success', 'Success');
        break;
      case 'error':
        NotificationManager.error('Please check your login and password', 'Error', 5000);
        break;
    }
};
const confirmeTest=(data)=>{
    axios
    .put(
       `https://localhost:5001/api/Test/${data.id}`,{
        "Id":parseInt(data.id),
       "CreatorId":parseInt(data.creatorId),
       "Name":data.name,
       "Description":data.description,
       "Type":data.type,
       "MaximumAttemptByUser":parseInt(data.maximumAttemptByUser),
       "Password":data.password,
        "Timer":parseInt(data.timer),
       "Status":"Confirmed",
       "isDeleated":data.isDeleated}).then(response => {
         createNotification('success')
         fetchData();
       }).catch(err=>
         {
           console.log(err)
           createNotification('error')
         })
}
const fetchData = () => { 
    axios
    .get(
       `https://localhost:5001/api/Test/WaitingConfirme`).then(response => {
        settestsWaitingConfrime(response.data)
       })}
       const deleteTest = (data) => { 
        axios
        .put(
           `https://localhost:5001/api/Test/${data.id}`,{
            "Id":parseInt(data.id),
           "CreatorId":parseInt(data.creatorId),
           "Name":data.name,
           "Description":data.description,
           "Type":data.type,
           "MaximumAttemptByUser":parseInt(data.maximumAttemptByUser),
           "Password":data.password,
            "Timer":parseInt(data.timer),
           "Status":data.status,
           "isDeleated":true}).then(response => {
             createNotification('success')
             fetchData();
           }).catch(err=>
             {
               console.log(err)
               createNotification('error')
             })}
useEffect(() => {
    fetchData();
   },[]);

    return(
<div>
{
testsWaitingConfrime.map((data)=>
        {
        return(
            <Grid  key={data.id} container >
                <Grid item xs={3} sm={3}>
                </Grid>
                <Grid item xs={6}>
            <Paper className={classes.paper}>
          <Grid container>
          <Grid item xs={4} >
        <h4 className={classes.about}>Test Id</h4>
        <h1 >{data.id}</h1>
        </Grid>
        <Grid item xs={4} >
        <h4 className={classes.about}>Test name</h4>
        <h1 >{data.name}</h1>
        </Grid>
        <Grid item xs={4}>
        <h4 className={classes.about}>Test description</h4>
        <h1 >{data.description}</h1>
        </Grid>
        <Grid item xs={4}>
        <h1 >{data.adress}</h1>
        <Button type="submit" variant="contained" onClick={()=>confirmeTest(data)}>Confirme test</Button>
        </Grid>
        <Grid item xs={4}>
        <h1 >{data.adress}</h1>
        <Link to={`/addQuestion/${data.id}`} className="btn btn-primary" ><Button type="submit" variant="contained" color="primary">Add questions</Button></Link>
        </Grid>
        <Grid item xs={4}>
        <h1 >{data.adress}</h1>
        <Button type="submit" variant="contained" color="secondary" onClick={()=>deleteTest(data)}>Delete test</Button>
        </Grid>
        
        </Grid>
        <Grid item xs={12}>
          </Grid>
        </Paper>
        </Grid>
        <NotificationContainer/>
      </Grid>
)
              })
              }
              


</div>
    )
}
export default ConfirmeTestComponent