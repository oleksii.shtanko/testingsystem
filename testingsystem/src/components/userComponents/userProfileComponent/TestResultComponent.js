import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import axios from "axios"
import Button from '@material-ui/core/Button';
import { Redirect } from "react-router-dom"
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      margin:theme.spacing(2),
      padding: theme.spacing(3),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    selectedPage:{
      margin:theme.spacing(2),
      padding: theme.spacing(3),
      textAlign: 'center',
       background: "#3d50b6",
       color:"white"
    },
    linkSelected:{
      color:"white"
    },
    btn:{
      marginTop:theme.spacing(2)
    }
  }));

const TestResultComponent =()=>{
   const classes = useStyles();
   const [testResult,setTestResult]=useState([])

   const getResults=()=>{
    axios
    .get(
       `https://localhost:5001/api/TestHistory/Test/${localStorage.getItem("id")}`).then(response => {
        setTestResult(response.data.reverse())
       })
   }
   useEffect(() => {
    getResults()
    },[]);

    return(
        <Grid container >
        <Grid item xs={3}>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>
              <Grid container>
                <Grid item xs={1}></Grid>
                <Grid item xs={5}>
                    <Link className={classes.linkSelected} to="/personalData"> 
                        <Paper className={classes.paper}>
                            Personal data
                        </Paper>
                    </Link>
                </Grid>
                <Grid item xs={5}>
                    <Link className={classes.linkSelected} to="/testResult"> 
                        <Paper className={classes.selectedPage}>
                            Test Results
                        </Paper>
                    </Link>
                </Grid>
            </Grid>
            <Grid container>
                
{
    testResult.map((data)=>
              {
                return(
                    <Grid  key={data.id} container >
                        <Grid item xs={3} >
                        </Grid>
                        <Grid item xs={6}>
                    <Paper className={classes.paper}>
                  <Grid container>
                  <Grid item xs={4} >
                <h4 >Test name</h4>
                <h1 >{data.name}</h1>
                </Grid>
                <Grid item xs={4} >
                <h4>Time</h4>
                <h1 >{data.timeFinish}</h1>
                </Grid>
                <Grid item xs={4}>
                <h4>You result</h4>
                <h1 >{data.score}</h1>
                </Grid>
                <Grid item xs={4}>
                </Grid>
                <Grid item xs={4}>  
                </Grid>
                <Grid item xs={4}>             
                </Grid>
                </Grid>
                <Grid item xs={12}>
                  </Grid>
                </Paper>
                </Grid>
              </Grid>
)
              })
              }
                
            </Grid>
        </Paper>
        </Grid>
        </Grid>
    )
}
export default TestResultComponent