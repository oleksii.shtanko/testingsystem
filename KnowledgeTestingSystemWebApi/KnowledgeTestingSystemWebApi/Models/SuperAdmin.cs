﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Models
{
    public class SuperAdmin
    {
        [Key]
        public int Id { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Login { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Password { get; set; }
    }
}
