import React,{useState,useEffect, useRef} from 'react';
import { Redirect } from "react-router-dom"
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import axios from "axios"
import {Link} from 'react-router-dom';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      margin:theme.spacing(3),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    paperImg: {
      padding: theme.spacing(2),
  
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    head:{
      marginBottom: theme.spacing(5),
      marginTop: theme.spacing(5),
      textAlign:'center',
      fontSize:"42px",
  
    },
    img:{
      maxWidth:"100%"
  
    },
    input:{
MarginTop:"30px"
    },
    about:{
      textAlign:'center',
      fontWeight:"bold"
    },
    main:{
     
    },
    button:{
      padding: theme.spacing(3),
  
        hight:"200px"
    }
  }));

const StartTestComponent=(props)=>{
 const classes = useStyles();
    const [questions, setQuestions] = useState([]);
    const [answers, setAnswers] = useState([]);
    const [answersData, setAnswersData] = useState([])
    const [disabled, setDisabled] = useState(false)
    const [timer, setTimer] = useState(1)
    const [currentTimer, setCurrentTimer] = useState(0)
    const getQuestions = () => { 
        axios
        .get(
           `https://localhost:5001/api/Question/GetQuestions/${props.match.params.id}`).then(response => {
            setQuestions(response.data)
           })}

    const getTime=()=>{
      axios
        .get(
           `https://localhost:5001/api/Test/${props.match.params.id}`).then(response => {
            setTimer(response.data.timer)
           })
    }     
        const sendQuestions=(data)=>{
            console.log(answersData)
                axios.post(
                    `https://localhost:5001/api/TestQuestionHistory`,
                      answersData
                    ).then(res=>{
                      axios.post(
                        `https://localhost:5001/api/TestQuestionHistory/Check`,
                          answersData
                        ).then(res=>{
                          setDisabled(true)  
                          createNotification('success')
                       }).catch(err=>
                        {
                          console.log(err)
                          createNotification('error')
                        })
                    })  
           
        }


    var createNotification = (type) => {
  
      switch (type) {
        case 'success':
          NotificationManager.success('Result sended', 'Success',1000);
          break;
        case 'error':
          NotificationManager.error('You need to answer at least 1 question', 'Error', 1000);
          break;
      }
  
  };
           const saveAnswer1=(data)=>{
            setAnswers({
                ...answers,
                questionId:data.id,
               isFirstCorrect:true,
               isSecondCorrect:false,
               isThirdCorrect:false,
               isFourthCorrect:false
              });
           }
           const saveAnswer2=(data)=>{
            setAnswers({
                ...answers,
                questionId:data.id,
                isSecondCorrect:true,
                isFirstCorrect:false,
                isThirdCorrect:false,
                isFourthCorrect:false
              });
            }
              const saveAnswer3=(data)=>{
                setAnswers({
                    ...answers,
                    questionId:data.id,
                    isSecondCorrect:false,
                    isFirstCorrect:false,
                    isThirdCorrect:true,
                    isFourthCorrect:false
                  });
           }
           const saveAnswer4=(data)=>{
            setAnswers({
                ...answers,
                questionId:data.id,
                isSecondCorrect:false,
                isFirstCorrect:false,
                isThirdCorrect:false,
                isFourthCorrect:true
              });
       }
           const answerQuestion = (data) => { 
             console.log(answersData)
               answersData.push(  {"testId":parseInt(props.match.params.id),
               "userId":parseInt(localStorage.getItem("id")),
               "questionId":parseInt(data.id),
               "isFirstCorrect":answers.isFirstCorrect,
               "isSecondCorrect":answers.isSecondCorrect,
               "isThirdCorrect":answers.isThirdCorrect,
               "isFourthCorrect":answers.isFourthCorrect,
               "Status":"Waiting"})
               }

             useInterval(() => {
                setCurrentTimer(currentTimer + 1);
              }, 1000);
                 
              function useInterval(callback, delay) {
                const savedCallback = useRef();

                useEffect(() => {
                  savedCallback.current = callback;
                }, [callback]);
              
                useEffect(() => {
                  function tick() {
                    savedCallback.current();
                  }
                  if (delay !== null) {
                    let id = setInterval(tick, delay);
                    return () => clearInterval(id);
                  }
                }, [delay]);
              }

    useEffect(() => {
       getTime();
        getQuestions();
        if(currentTimer/60>=timer){
          setDisabled(true)
        }
        },[currentTimer]);
        
        const checkDisable=(id)=>{
          let disable = false
          answersData.forEach(element => {
            if(element.questionId==id)
            {
              disable= true
            }
          });
        return disable;
        }
    return(
        <div>
          <h1>{parseInt(currentTimer/60)}:{currentTimer%60} /{timer} min</h1>
    {
        questions.map((data)=>
        
                  {
                    return(
                        <Grid  key={data.id} container >
                            <Grid item xs={3} >
                            </Grid>
                            <Grid item xs={6}>
                        <Paper className={classes.paper}>
                      <Grid container>
                      <Grid item xs={12} >
                    <h4 className={classes.about}>Question</h4>
                    <h1 >{data.question}</h1>
                    </Grid>
                    <Grid xs={2}>
                    </Grid>
                    <Grid xs={8}>
                        <div>
                            <div onChange={()=>saveAnswer1(data)}>
                    {
                    data.firstAnswer==null ||data.firstAnswer==="" ?"":<Grid container><Grid item xs={1}>
                    
                    <div className={classes.about} >
                    <input type="radio" value="true" name={data.id} /> 
                    </div>
                    </Grid>
                    <Grid item xs={11} >
                    <h4>{data.firstAnswer}</h4>
                    </Grid>
                    </Grid>

                    }
                    </div>
                    <div onChange={()=>saveAnswer2(data)}>
                    {data.secondAnswer==null ||data.secondAnswer==="" ?"":<Grid container><Grid item xs={1}>
           
                    <div className={classes.about}>
                    <input  type="radio" value="true" name={data.id} /> 
                    </div>
                    </Grid>
                    <Grid item xs={11} >
                    <h4>{data.secondAnswer}</h4>
                    </Grid>
                    </Grid>
                    }
                    </div>
                    <div onChange={()=>saveAnswer3(data)}>
                    {
                    data.thirdAnswer==null ||data.thirdAnswer==="" ?"":<Grid container><Grid item xs={1}>
                 
                    <div className={classes.about}>
                    <input  type="radio" value="true" name={data.id} /> 
                    </div>
                    </Grid>
                    <Grid item xs={11} >
                    <h4>{data.thirdAnswer}</h4>
                    </Grid>
                    </Grid>
                    }
                    </div>
                    <div onChange={()=>saveAnswer4(data)}>
                    {
                    data.fourthAnswer==null ||data.fourthAnswer==="" ?"":<Grid container><Grid item xs={1}>
                 
                    <div className={classes.about}>
                    <input  type="radio" value="true" name={data.id} /> 
                    </div>
                    </Grid>
                    <Grid item xs={11} >
                    <h4>{data.fourthAnswer}</h4>
                    </Grid>
                    </Grid>
                    }
                    </div>
                    </div>
                    </Grid>
                    <Grid xs={2}>
                    </Grid>
                    </Grid>
                    <Grid item xs={12}>
                    <Button disabled={checkDisable(data.id)} onClick={()=>answerQuestion(data)} type="submit" variant="contained" color="primary">Answer</Button>
                      </Grid>
                    </Paper>
                    </Grid>
                  </Grid>
    )
                  })
                  }
                  <Grid container>
                    <Grid item xs={5}></Grid>
                    <Grid item xs={2}>
                    {(currentTimer/60>=timer)?<Button fullWidth  type="submit" variant="contained" color="secondary">Time end</Button>:<Button fullWidth disabled={disabled} onClick={sendQuestions} type="submit" variant="contained" color="primary">Finish test</Button>}
                  </Grid>
                  <Grid item xs={5}></Grid>
                  </Grid>
                  <NotificationContainer/>
        </div>
    )
}

export default StartTestComponent