import React,{ useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from "axios"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { Redirect } from "react-router-dom"
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginPage=({userAuth,userLogin,Admin,...props})=> {
  const classes = useStyles();

  const initialFieldValues={
    login:'',
    password:'',
}
const [UserData, setUserData] = useState(initialFieldValues)
const [DirtyLogin,setDirtyLogin]=useState(false)
const [loginError,setLoginError]=useState("Login is not correct")
const [DirtyPassword,setDirtyPassword]=useState(false)
const [passwordError,setPasswordError]=useState("Password is not correct")
const [formValid,setFromValid]=useState(false)
const handleChangeLogin = (event) => {
    const re = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
    if(!re.test(String(event.target.value).toLowerCase())){
      setLoginError("Login is not correct")
    }
    else{
      setLoginError("")
    }
    setUserData({
      ...UserData,
      login:event.target.value,
    });
  };
  const handleChangePassword = (event) => {
    const re = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if(!re.test(String(event.target.value).toLowerCase())){
      setPasswordError("Password is not correct")
    }
    else{
      setPasswordError("")
    }
    setUserData({
      ...UserData,
      password:event.target.value,
    });
  };

  var createNotification = (type) => {
  
    switch (type) {
      case 'success':
        NotificationManager.success('Success', 'Success',500);
        break;
      case 'error':
        NotificationManager.error('Please check your login and password', 'Error', 500);
        break;
    }
};
  const handleSubmit=(e)=>
  {
   e.preventDefault();
    axios.get(
       `https://localhost:5001/api/User?username=${UserData.login}&pass=${UserData.password}`,
        {withCredentials:true}).then(response => {
          localStorage.setItem('token', response.data.token)
          axios.post(
           `https://localhost:5001/api/User/Post`,
            {withCredentials:true},{
            headers: { Authorization:`Bearer ${response.data.token}`} }
                  ).then(res=>{
                    localStorage.setItem('id', res.data)
                    userAuth.setLoggedIn(userAuth.loggedIn=true)
                    createNotification('success')
                        }).catch(err=>
                          {
                            createNotification('error')
                          })
               }).catch(err=>
                {
                  createNotification('error')
                })
  }
const handleSubmitAdmin=(e)=>
{
 e.preventDefault();
 axios
    .get(
       `https://localhost:5001/api/Admin?username=${UserData.login}&pass=${UserData.password}`,{withCredentials:true}).then(response => {
        localStorage.setItem('admintoken', response.data.token)
        axios
        .post(
           `https://localhost:5001/api/Admin/Post`,
           {withCredentials:true},{
           headers: { Authorization:`Bearer ${response.data.token}`} }
        ).then(res=>{
          localStorage.setItem('adminId', res.data)
          Admin.setloggedAdmin(Admin.loggedAdmin=true)
        })  
       })
}

  useEffect(() => {
    if(loginError ||passwordError){
      setFromValid(false)
    }
    else{
      setFromValid(true)
    }
  },[loginError,passwordError]);


  const blurHandler=(e)=>{
switch(e.target.name){
  case 'login':
    setDirtyLogin(true)
break
case 'password':
  setDirtyPassword(true)
break
}
  }
  if(userAuth.loggedIn==true )
  return(
      <Redirect to="/"></Redirect>
     )
     if(Admin.loggedAdmin==true )
     return(
         <Redirect to="/"></Redirect>
        )
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
        {(DirtyLogin && loginError)&&<div style={{color:'red'}}>{loginError}</div>}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="login"
            label="Login"
            name="login"
            autoComplete="login"
            autoFocus
            value={UserData.login}
            onChange={handleChangeLogin}
            onBlur={e=>blurHandler(e)}
          />
           {(DirtyPassword && passwordError)&&<div style={{color:'red'}}>{passwordError}</div>}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={UserData.password}
            onChange={handleChangePassword}
            onBlur={e=>blurHandler(e)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
            disabled={!formValid}
          >
            Sign In
          </Button>
          <Button
           fullWidth
           variant="contained"
           color="primary"
           className={classes.submit}
           onClick={handleSubmitAdmin}
         >
           Sign In as admin
         </Button>
         <Button
           fullWidth
           variant="contained"
           color="primary"
           className={classes.submit}
           onClick={handleSubmitAdmin}
         >
           Sign In as super admin
         </Button>
          <Grid container alignItems="center"
  justify="center">
            <Grid item>
              <Link to="/registration" variant="body2">
                Don't have an account? Sign Up
              </Link>
            </Grid>
          </Grid>
        </form>
        <NotificationContainer/>
      </div>
    </Container>
  );
}
export default LoginPage
