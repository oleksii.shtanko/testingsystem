import React from "react"
import { Redirect } from "react-router-dom"

const Logout=(props)=>{
props.userAuth.setLoggedIn(props.userAuth.loggedIn=false)
props.Admin.setloggedAdmin(props.Admin.loggedAdmin=false)
localStorage.setItem('token', null)
localStorage.setItem('admintoken', null)
localStorage.setItem('id', null)
return(
 <Redirect to="/"></Redirect>
)
    }

export default Logout