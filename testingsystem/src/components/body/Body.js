import React,{useState,useEffect} from 'react';
import Header from '../header/Header'
import Footer from '../footer/Footer'
import Sections from '../sections/Sections';
import axios from "axios"
const Body=()=>{
    const [loggedIn, setLoggedIn] = useState(false)
    const [loggedAdmin, setloggedAdmin] = useState(false)
    useEffect(() => {
        if(localStorage.getItem('token')!=null) 
        {
          axios
        .post(
           `https://localhost:5001/api/User/Post`,
           {withCredentials:true},{
           headers: { Authorization:`Bearer ${localStorage.getItem('token')}`} }
        ).then(res=>{   
      setLoggedIn(true)
        })
        }
        if(localStorage.getItem('admintoken')!=null) 
        {
          axios
        .post(
          `https://localhost:5001/api/Admin/Post`,
           {withCredentials:true},{
           headers: { Authorization:`Bearer ${localStorage.getItem('admintoken')}`} }
        ).then(res=>{   
          setloggedAdmin(true)
        })
        }
      });
    return(
<div>
<Header userAuth={{ loggedIn, setLoggedIn}} Admin={{loggedAdmin, setloggedAdmin}}/>
<Sections userAuth={{ loggedIn, setLoggedIn}} Admin={{loggedAdmin, setloggedAdmin}} />
<Footer/>
</div>
    )
}
export default Body;