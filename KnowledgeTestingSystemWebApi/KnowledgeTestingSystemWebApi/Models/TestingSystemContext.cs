﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Models
{
    public class TestingSystemContext: DbContext
    {
        public TestingSystemContext(DbContextOptions<TestingSystemContext> options) : base(options)
        {

        }
        public DbSet<User> DbUsers { get; set; }
        public DbSet<Admin> DbAdmins { get; set; }
        public DbSet<SuperAdmin> DbSuperAdmins { get; set; }
        public DbSet<TestQuestion> DbQuestions { get; set; }
        public DbSet<Test> DbTests { get; set; }
        public DbSet<TestHistory> DbTestHistories { get; set; }

    }
}
