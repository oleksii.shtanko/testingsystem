import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from "axios"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));
const AddQuestionComponent =(props)=>{
    const classes = useStyles();
    const initialFieldValues={
        type:'',
        question:'',
        firstAnswer:'',
        secondAnswer:'',
        thirdAnswer:'',
        fourthAnswer:'',
        isFirstCorrect:false,
        isSecondCorrect:false,
        isThirdCorrect:false,
        isFourthCorrect:false,
        maximumScoreExist:0,
    }
    const [questionData, setQuestionData] = useState(initialFieldValues)
    
    const handleChangeType=(event)=> {
        setQuestionData({
            ...questionData,
            type:event.target.value,
          });
      }

      const handleChangeQuestion=(event)=>{
        setQuestionData({
            ...questionData,
            question:event.target.value,
          });
      }
      const handleChangeFirstAnswer=(event)=>{
        setQuestionData({
            ...questionData,
            firstAnswer:event.target.value,
          });
      }
      const handleChangeSecondAnswer=(event)=>{
        setQuestionData({
            ...questionData,
            secondAnswer:event.target.value,
          });
      }
      const handleChangeThirdAnswer=(event)=>{
        setQuestionData({
            ...questionData,
            thirdAnswer:event.target.value,
          });
      }
      const handleChangeFourthAnswer=(event)=>{
        setQuestionData({
            ...questionData,
            fourthAnswer:event.target.value,
          });
      }
      const handleChangeIsFirstCorrect=(event)=>{
        setQuestionData({
            ...questionData,
            isFirstCorrect:Boolean(event.target.value),
          });
      }
      const handleChangeIsSecondCorrect=(event)=>{
        setQuestionData({
            ...questionData,
            isSecondCorrect:Boolean(event.target.value),
          });
      }
      const handleChangeIsThirdCorrect=(event)=>{
        setQuestionData({
            ...questionData,
            isThirdCorrect:Boolean(event.target.value),
          });
      }
      const handleChangeIsFourthCorrect=(event)=>{
        setQuestionData({
            ...questionData,
           isFourthCorrect:Boolean(event.target.value),
          });
      }
      var createNotification = (type) => {
  
        switch (type) {
          case 'success':
            NotificationManager.success('Success', 'Success');
            break;
          case 'error':
            NotificationManager.error('Please check your login and password', 'Error', 5000);
            break;
        }
    };
      const addQuestion=(e)=>{
        e.preventDefault();
        axios
        .post(
           `https://localhost:5001/api/Question`,
           {
            "testId":parseInt(props.match.params.id),
            "type":questionData.type,
            "question":questionData.question,
            "firstAnswer":questionData.firstAnswer,
            "secondAnswer":questionData.secondAnswer,
            "thirdAnswer":questionData.thirdAnswer,
            "fourthAnswer":questionData.fourthAnswer,
            "isFirstCorrect":questionData.isFirstCorrect,
            "isSecondCorrect":questionData.isSecondCorrect,
            "isThirdCorrect":questionData.isThirdCorrect,
            "isFourthCorrect":questionData.isFourthCorrect,
            "maximumScoreExist":parseInt(questionData.maximumScoreExist),
            "isDeleated":false,
           }
        ).then(res=>{
      createNotification('success')
        }).catch(err=>
        {
          createNotification('error')
        })
      }

return (
    <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={2}>
            <Paper className={classes.paper}>xs=12</Paper>
          </Grid>
          <Grid item xs={8}>
            <Paper className={classes.paper}>
            <Grid container >
                <Grid item xs={3}></Grid>   
                <Grid item xs={6}>
                    <h1>Add question</h1>
                    <h3>Question type</h3>
                    <div onChange={handleChangeType}>
        <input  type="radio" value="Single" name="type" /> Single answer
        <input type="radio" value="Multi" name="type" /> Multi answer
      </div>
            <h3>Question</h3>
            <TextField
          id="test-description"
          label="Multiline"
          fullWidth
          required
          multiline
          rows={4}
          variant="outlined"
          value={questionData.question}
          onChange={handleChangeQuestion}
          />
             <h3>1 Answer</h3>
            <TextField
          required
          fullWidth
          id="outlined-required-test"
          variant="outlined"
          value={questionData.firstAnswer}
          onChange={handleChangeFirstAnswer}/>
          
                    <div onChange={handleChangeIsFirstCorrect}>
        <input type="radio" value="true" name="answer1" /> Correct
        <input type="radio" value=""  name="answer1" /> Wrong
      </div>
           <h3>2 Answer</h3>
            <TextField
          required
          fullWidth
          id="outlined-required-test"
          variant="outlined"
          value={questionData.secondAnswer}
          onChange={handleChangeSecondAnswer}/>
                  <div onChange={handleChangeIsSecondCorrect}>
        <input type="radio" value="true" name="answer2" /> Correct
        <input type="radio" value=""  name="answer2" /> Wrong
      </div>
           <h3>3 Answer</h3>
            <TextField
          required
          fullWidth
          id="outlined-required-test"
          variant="outlined"
          value={questionData.thirdAnswer}
          onChange={handleChangeThirdAnswer}/>
                  <div onChange={handleChangeIsThirdCorrect}>
        <input type="radio" value="true" name="answer3" /> Correct
        <input type="radio" value=""  name="answer3" /> Wrong
      </div>
           <h3>4 Answer</h3>
            <TextField
          required
          fullWidth
          id="outlined-required-test"
          variant="outlined"
          value={questionData.fourthAnswer}
          onChange={handleChangeFourthAnswer}/>
                  <div onChange={handleChangeIsFourthCorrect}>
        <input type="radio" value="true" name="answer4" /> Correct
        <input type="radio" value=""  name="answer4" /> Wrong
      </div>
                </Grid>
                      <Grid container>
                      <Grid item xs={5}>
                      </Grid>
                      <Grid item xs={2}>
                      <Button
                     type="submit"
                     fullWidth
                     variant="contained"
                    color="primary"
                    onClick={addQuestion}
                    >
                      Add question
                     </Button>
                      </Grid>
                      <Grid item xs={5}>
                      </Grid>
            <Grid item xs={3}></Grid>
        </Grid>
       
        <Grid item xs={3}></Grid>
        </Grid>
            </Paper>
          </Grid>
          <Grid item xs={2}>
            <Paper className={classes.paper}>xs=6</Paper>
          </Grid>
          <NotificationContainer/>
        </Grid>
      </div>
)
}
export default AddQuestionComponent