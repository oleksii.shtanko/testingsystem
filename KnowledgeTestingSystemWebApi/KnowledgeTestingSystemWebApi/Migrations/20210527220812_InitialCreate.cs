﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KnowledgeTestingSystemWebApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbAdmins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    FullName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    IsDeleated = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbAdmins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbQuestions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TestId = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(20)", nullable: true),
                    Question = table.Column<string>(type: "nvarchar(300)", nullable: true),
                    FirstAnswer = table.Column<string>(type: "nvarchar(300)", nullable: true),
                    SecondAnswer = table.Column<string>(type: "nvarchar(300)", nullable: true),
                    ThirdAnswer = table.Column<string>(type: "nvarchar(300)", nullable: true),
                    FourthAnswer = table.Column<string>(type: "nvarchar(300)", nullable: true),
                    IsFirstCorrect = table.Column<bool>(type: "BIT", nullable: false),
                    IsSecondCorrect = table.Column<bool>(type: "BIT", nullable: false),
                    IsThirdCorrect = table.Column<bool>(type: "BIT", nullable: false),
                    IsFourthCorrect = table.Column<bool>(type: "BIT", nullable: false),
                    MaximumScoreExist = table.Column<int>(type: "int", nullable: false),
                    IsDeleated = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbQuestions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbSuperAdmins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbSuperAdmins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbTestHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Score = table.Column<int>(type: "int", nullable: false),
                    TimeFinish = table.Column<string>(type: "nvarchar(Max)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTestHistories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbTests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    MaximumAttemptByUser = table.Column<int>(type: "int", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    PassingScore = table.Column<int>(type: "int", nullable: false),
                    MaximumScore = table.Column<int>(type: "int", nullable: false),
                    IsDeleated = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    FullName = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Sex = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Age = table.Column<int>(type: "int", nullable: false),
                    IsDeleated = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbUsers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbAdmins");

            migrationBuilder.DropTable(
                name: "DbQuestions");

            migrationBuilder.DropTable(
                name: "DbSuperAdmins");

            migrationBuilder.DropTable(
                name: "DbTestHistories");

            migrationBuilder.DropTable(
                name: "DbTests");

            migrationBuilder.DropTable(
                name: "DbUsers");
        }
    }
}
