﻿using KnowledgeTestingSystemWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeTestingSystemWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly TestingSystemContext _context;

        public UserController(IConfiguration config, TestingSystemContext context)
        {
            _config = config;
            _context = context;
        }
        [HttpGet]
        public IActionResult Login(string username, string pass)
        {
            User auth = new User
            {
                Login = username,
                Password = pass
            };
            IActionResult response = Unauthorized();

            var user = AuthenticateUser(auth);
            if (user != null)
            {
                var tokenStr = GenerateJSONWebToken(user);
                response = Ok(new { token = tokenStr });
            }
            return response;
        }
        private User AuthenticateUser(User auth)
        {
            User user = null;
            byte[] ByteData = Encoding.ASCII.GetBytes(auth.Password);
            MD5 md5 = MD5.Create();
            byte[] HashData = md5.ComputeHash(ByteData);
            StringBuilder oSb = new StringBuilder();
            for (int x = 0; x < HashData.Length; x++)
            {
                oSb.Append(HashData[x].ToString("x2"));
            }
            auth.Password = oSb.ToString();
            User userData = _context.DbUsers.FirstOrDefault(u => u.Login == auth.Login &&
            u.Password == auth.Password);
            if (userData != null)
            {
                if (auth.Login == userData.Login && auth.Password == userData.Password && userData.IsDeleated == false  )
                {
                    user = new User { Login = userData.Login, Password = userData.Password, Id = userData.Id };
                }
                return user;
            }
            return null;
        }


        private string GenerateJSONWebToken(User userinfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,userinfo.Login),
                new Claim(JwtRegisteredClaimNames.Email,userinfo.Id.ToString())
        };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }
        [Authorize]
        [HttpPost("Post")]
        public string Post()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claim = identity.Claims.ToList();
            var userName = claim[1].Value;
            return userName;
        }

        private bool DUserExists(int id)
        {
            return _context.DbUsers.Any(e => e.Id == id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDUser(int id, User dUser)
        {
            if (id != dUser.Id)
            {
                return BadRequest();
            }

            _context.Entry(dUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        [HttpPost("CreateUser")]
        public async Task<ActionResult<User>> PostUser(User dUser)
        {
            User isLoginExcist = _context.DbUsers.FirstOrDefault(u => u.Login == dUser.Login);
            if (isLoginExcist == null)
            {
                byte[] ByteData = Encoding.ASCII.GetBytes(dUser.Password);
                MD5 md5 = MD5.Create();
                byte[] HashData = md5.ComputeHash(ByteData);
                StringBuilder oSb = new StringBuilder();
                for (int x = 0; x < HashData.Length; x++)
                {
                    oSb.Append(HashData[x].ToString("x2"));
                }
                dUser.Password = oSb.ToString();
                _context.DbUsers.Add(dUser);
                await _context.SaveChangesAsync();
                return CreatedAtAction("PostUser", new { id = dUser.Id }, dUser);
            }
            return BadRequest("Login already exists.");
        }
    }
}
