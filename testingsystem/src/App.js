import './App.css';
import {BrowserRouter as Switch} from "react-router-dom";
import Body from '../src/components/body/Body'
function App() {
  return (
    <div>
      <Switch>
        <Body/>
      </Switch>
    </div>
  );
}

export default App;
