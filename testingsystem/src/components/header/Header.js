import React, { Component } from "react";
import {
    Nav,
    NavLink,
    Bars,
    NavMenu,
    NavBtn,
    NavBtnLink,
  } from './NavBarElements.js';

const Header=({userAuth,Admin,...props})=>{

  if(userAuth.loggedIn==false && Admin.loggedAdmin==false)
      return (     
        <>
     <Nav>
      <Bars />
      <NavMenu>
        <NavLink exact  to='/'>
        <h3>  Home</h3> 
        </NavLink>
      </NavMenu>
      <NavBtn>
        <NavBtnLink to='/login'>Sign In</NavBtnLink>
      </NavBtn>
    </Nav>
    <hr style={{size:"30px",color:"#31373e"}}></hr>
      </>
        )
  if(userAuth.loggedIn==true)
  return (      
    <>
    
    <Nav>
      <Bars />
       <NavMenu>
        <NavLink exact to='/' >
        <h3>  Home</h3> 
        </NavLink>
        <NavLink to='/tests'>
        <h3> Tests</h3>
        </NavLink>
        <NavLink to='/personalData' >
        <h3> Profile</h3> 
        </NavLink>
      </NavMenu>
      <NavBtn>
        <NavBtnLink to='/logout'>Logout</NavBtnLink>
      </NavBtn>
    </Nav>
    <hr style={{size:"30px",color:"#31373e"}}></hr>
  </>)
   if(Admin.loggedAdmin==true)
   return (      
     <>
     <Nav>
       <Bars />
       <NavMenu>
         <NavLink exact to='/'>
         <h3>Home</h3> 
         </NavLink>
         <NavLink to='/createTest' >
         <h3>Create Test</h3> 
         </NavLink>
         <NavLink to='/confirmeTests' >
         <h3>Confirme Tests</h3> 
         </NavLink>
         <NavLink to='/charts' >
         <h3>  Charts</h3>
         </NavLink>
         <NavLink to='/adminPanel' >
         <h3>Admin Panel</h3>
         </NavLink>
       </NavMenu>
       <NavBtn>
       <NavBtnLink to='/logout'>Logout</NavBtnLink>
       </NavBtn>
     </Nav>
     <hr style={{size:"30px",color:"#31373e"}}></hr>
   </>)
  return (
    <>
    <Nav>
      <Bars />
         <NavMenu>
           <NavLink exact to='/' >
            <h3>Home</h3> 
            </NavLink>
          </NavMenu>
            <NavBtn>
            <NavBtnLink to='/login'>Sign In</NavBtnLink>
            </NavBtn>
      </Nav>
      <hr style={{size:"30px",color:"#31373e"}}></hr>
      </>
    );
}

export default Header;